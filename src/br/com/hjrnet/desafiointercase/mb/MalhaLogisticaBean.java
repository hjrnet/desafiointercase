package br.com.hjrnet.desafiointercase.mb;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;




//import org.primefaces.model.chart.PieChartModel;




import br.com.hjrnet.desafiointercase.dao.MalhaLogisticaDao;
import br.com.hjrnet.desafiointercase.modelo.MalhaLogistica;

/**
 * @author hjrnet
 * Controle de persistencia, CRUD.
 */
@ManagedBean
public class MalhaLogisticaBean implements Serializable {

	private static final long serialVersionUID = -1957176543273170800L;

	private List<MalhaLogistica> malhas;

	private MalhaLogistica malha = new MalhaLogistica();
	
	private Long malhaId;
	
	
	private MalhaLogisticaDao dao = new MalhaLogisticaDao();

	public String grava(){
		MalhaLogisticaDao dao = new MalhaLogisticaDao();
		if (malha.getId() == null){
			dao.adiciona(malha);
		} else {
			dao.atualiza(malha);
		}		
	
	     this.malhas = dao.listaTodos();

	     this.malha=new MalhaLogistica();
		
		return "index?faces-redirect=true";
	}
	
	public void carregaMalha(){
		if (malhaId != null && malhaId != 0){
			this.malha = dao.buscaPorId(this.malhaId);
		}
	}
	
	public List<MalhaLogistica> getMalhaLogisticas(){
		if (malhas==null){
			System.out.println("Carregando produtos...");
			malhas = new MalhaLogisticaDao().listaTodos();
		
		}
		return malhas;
	}
	
	public void remove(MalhaLogistica malha){
		MalhaLogisticaDao dao = new MalhaLogisticaDao();
		
		dao.remove(malha);
		this.malhas = dao.listaTodos();
		this.malha = new MalhaLogistica();
	}
	
	public void limpa(){
		MalhaLogisticaDao dao = new MalhaLogisticaDao();
		
		this.malhas = dao.listaTodos();
		this.malha = new MalhaLogistica();
	}

	
	public String cancela(){
		System.out.println("Cancela edição");
		this.malha = new MalhaLogistica();
		return "malha";
	}


	public List<MalhaLogistica> getMalhas() {
		return malhas;
	}


	public MalhaLogistica getMalha() {
		return malha;
	}


	public void setMalha(MalhaLogistica malha) {
		this.malha = malha;
	}


	public Long getMalhaId() {
		return malhaId;
	}


	public void setMalhaId(Long malhaId) {
		this.malhaId = malhaId;
	}


	public MalhaLogisticaDao getDao() {
		return dao;
	}


	public void setDao(MalhaLogisticaDao dao) {
		this.dao = dao;
	}


	public void setMalhas(List<MalhaLogistica> malhas) {
		this.malhas = malhas;
	}


}
