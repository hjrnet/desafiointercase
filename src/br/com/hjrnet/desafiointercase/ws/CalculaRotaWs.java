package br.com.hjrnet.desafiointercase.ws;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.hjrnet.desafiointercase.controle.CalculaRota;

/**
 * @author hjrnet
 *IMplementado o Webservice RestFul
 */
@Path("/calcularota")
public class CalculaRotaWs {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello(@QueryParam("nome") String nome,@QueryParam("origem") String origem,
	@QueryParam("destino") String destino, @QueryParam("autonomia") String autonomia , @QueryParam("valorlitro") String valorlitro) {
	
		CalculaRota clcR =new CalculaRota();
		clcR.calculaRota(nome,origem,destino, Double.parseDouble(autonomia),Double.parseDouble(valorlitro));
			
		return "Rota: " + clcR.path+ ",  total custo: " + clcR.total+" "+clcR.retorno ;
	}

	@GET
	@Produces(MediaType.TEXT_XML)
	public String sayXMLHello(@QueryParam("nome") String nome,@QueryParam("origem") String origem,
	@QueryParam("destino") String destino, @QueryParam("autonomia") String autonomia , @QueryParam("valorlitro") String valorlitro) {
	
		CalculaRota clcR =new CalculaRota();
		clcR.calculaRota(nome,origem,destino, Double.parseDouble(autonomia),Double.parseDouble(valorlitro));
			

		return "<?xml version=\"1.0\"?>" + "<hello> Rota: " + clcR.path+ ",  total custo: " + clcR.total
				+" "+ clcR.retorno +"</hello>";
	}

	@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHtmlHello(@QueryParam("nome") String nome,@QueryParam("origem") String origem,
	@QueryParam("destino") String destino, @QueryParam("autonomia") String autonomia , @QueryParam("valorlitro") String valorlitro) {
	
		CalculaRota clcR =new CalculaRota();
		clcR.calculaRota(nome,origem,destino, Double.parseDouble(autonomia),Double.parseDouble(valorlitro));
			
    
		return "<html> " + "<title>" + "Hjrnet Desafio Intercase"
				+ "</title>" + "<body><h1>" +" Rota: " +  clcR.path+ ",  total custo: " +  clcR.total
				+ "</body></h1>" +  clcR.retorno +"</html> ";
	}

}