package br.com.hjrnet.desafiointercase.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import br.com.hjrnet.desafiointercase.modelo.MalhaLogistica;
import br.com.hjrnet.desafiointercase.util.JPAUtil;

/**
 * @author hjrnet
 * tradutor do mundo SQL, e Unifica o acesso ao dados.
 */
public class MalhaLogisticaDao implements Serializable{

	private static final long serialVersionUID = 1L;
	public void adiciona(MalhaLogistica malha) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();

		//persiste o objeto
		manager.persist(malha);
		
		manager.getTransaction().commit();
		manager.close();
	}

	public void remove(MalhaLogistica malha) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();

		manager.remove(manager.merge(malha));

		manager.getTransaction().commit();
		manager.close();
	}

	public void atualiza(MalhaLogistica malha) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();

		manager.merge(malha);
		
		manager.getTransaction().commit();
		manager.close();
	}

	public List<MalhaLogistica> buscaPorNome(String nome) {

		EntityManager manager = new JPAUtil().getEntityManager();

		String jpql = "select m from MalhaLogistica m where "
				+ " lower(m.nome) like :nome order by m.nome";

		List<MalhaLogistica> lista = manager.createQuery(jpql, MalhaLogistica.class)
				.setParameter("nome", nome + "%").getResultList();

		manager.close();
		
		return lista; 
	}

	public List<MalhaLogistica> listaTodos() {
		EntityManager manager = new JPAUtil().getEntityManager();
		
		CriteriaQuery<MalhaLogistica> query = manager.getCriteriaBuilder().createQuery(MalhaLogistica.class);
		query.select(query.from(MalhaLogistica.class));

		List<MalhaLogistica> lista = manager.createQuery(query).getResultList();
	
		manager.close();
		
		return lista; 
	}
	
	public MalhaLogistica buscaPorId(Long id) {
		EntityManager manager = new JPAUtil().getEntityManager();

		MalhaLogistica produto = manager.find(MalhaLogistica.class, id);

		manager.close();

		return produto;
	}
	
}