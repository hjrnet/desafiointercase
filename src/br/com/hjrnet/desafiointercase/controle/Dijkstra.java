package br.com.hjrnet.desafiointercase.controle;

import java.util.PriorityQueue;
/**
 * @author hjrnet
 *Implemento o algoritmo de Dijkstra, concebido pelo cientista da computação holandês Edsger Dijkstra
 * em 1956 e publicado em 1959
 */
public class Dijkstra
{
    public void computePaths(Vertex source)
    {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
    vertexQueue.add(source);

    while (!vertexQueue.isEmpty()) {
        Vertex u = vertexQueue.poll();
  
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
        if (distanceThroughU < v.minDistance) {
            vertexQueue.remove(v);

            v.minDistance = distanceThroughU ;
            v.previous = u;
            vertexQueue.add(v);
        }
            }
        }
    }
}
