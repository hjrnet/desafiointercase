package br.com.hjrnet.desafiointercase.controle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;



import br.com.hjrnet.desafiointercase.controle.Dijkstra;
import br.com.hjrnet.desafiointercase.controle.Edge;
import br.com.hjrnet.desafiointercase.controle.Vertex;
import br.com.hjrnet.desafiointercase.mb.MalhaLogisticaBean;


/**
 * @author hjrnet
 *Junta os dados do banco com controle, que possui a funcao de calcular rota.
 */
public class CalculaRota {
	public List<Vertex> path;
	public double total;
	public String retorno;
	public String calculaRota(String nome, String origem, String destino, Double autonomia, Double valorlitro)
    {
		
			
		
	MalhaLogisticaBean mb = new MalhaLogisticaBean();
	java.util.Map<String, Vertex> map = new HashMap<>();
	for(int j = 0; j < mb.getMalhaLogisticas().size(); j++) {
		if (nome.equals(mb.getMalhaLogisticas().get(j).getNome())){
	for(int i = 0; i < mb.getMalhaLogisticas().size(); i++) {
	map.put( mb.getMalhaLogisticas().get(i).getOrigem(), 
			new Vertex( mb.getMalhaLogisticas().get(i).getOrigem()));
	map.put( mb.getMalhaLogisticas().get(i).getDestino(), 
			new Vertex( mb.getMalhaLogisticas().get(i).getDestino()));
	}
	
	for(int i = 0; i < mb.getMalhaLogisticas().size(); i++) {
	if (i<=1){
	map.get(mb.getMalhaLogisticas().get(i).getOrigem()).adjacencies = 
	new Edge[] { new Edge(map.get(mb.getMalhaLogisticas().get(i).getDestino()), 
			mb.getMalhaLogisticas().get(i).getDistancia()) };  
	}else{
	map.get(mb.getMalhaLogisticas().get(i).getDestino()).adjacencies = 
	new Edge[] { new Edge(map.get(mb.getMalhaLogisticas().get(i).getOrigem()), 
			mb.getMalhaLogisticas().get(i).getDistancia()) };  
	}
	System.out.println( map.get(mb.getMalhaLogisticas().get(i).getDestino()));
	}
		}
		else{
			return retorno=" Nome nao encontrado ";
		}
		}
	Dijkstra di=new Dijkstra();
	di.computePaths(map.get(origem)); // run Dijkstra
	 
   System.out.println("Distance to " +  map.get(destino) + ": " +  map.get(destino).minDistance);
   total = ( map.get("D").minDistance / autonomia) * valorlitro; 
   System.out.println("total custo: " + total);
   path = getShortestPathTo(map.get(destino));
   System.out.println("Path: " + path);
   return retorno="valido";
};
public static List<Vertex> getShortestPathTo(Vertex target)
{
    List<Vertex> path = new ArrayList<Vertex>();
    for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
        path.add(vertex);

    Collections.reverse(path);
    return path;
}
}
