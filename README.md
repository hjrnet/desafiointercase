Desenvolver um sistema de entregas visando sempre o menor custo. Para popular sua base de dados o sistema precisa expor um Webservices que aceite o formato de malha logística (exemplo abaixo) e nesta mesma requisição o requisitante deverá informar um nome para este mapa. É importante que os mapas sejam persistidos para evitar que a cada novo deploy todas as informações desapareçam. O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem, ponto de destino e distância entre os pontos em quilômetros.

A B 10

B D 15

A C 20

C D 30

B E 50

D E 30

Um exemplo de entrada seria, SP, origem A, destino D, autonomia 10, valor do litro 2,50; a resposta seria a rota A B D com custo de 6,75.

# * **Motivação** #

A Linguagem Java, Jsf e hibernate diminui a complexidade do desenvolvimento com menos código de infra e mais abstração do código de modelo e negócio. Além disso, implementei Dijkstra, que é um algoritmo otimizado para a busca do menor caminho.


# * **Requisitos** #

* Java 8
* GlassFish 4
* Mysql
* PrimeFaces 4

## * **Como utilizar** ##

1. baixar glassfish e instalar.
2. criar um banco mysql chamado desafiointercase.
3. clonar o projeto.
4. Compilar.
5. para acessar pagina de inserir  malha -> index.xhtml.
para testar a resposta por webservice RestFul acesse:


http://localhost:8080/hjrnet-desafio-intercase/rest/calcularota?nome=SP&origem=A&destino=D&autonomia=10&valorlitro=2.50
